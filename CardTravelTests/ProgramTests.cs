﻿using CardTravel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using static CardTravel.Program;

namespace CardTravelTests
{
    [TestClass]

    public class ProgramTests
    {
        [TestMethod]

        public void SortTravel_RegularArray_Sorted()
        {
            Card[] cards = new Card[]
            {
                new Card("Melbourne", "Cologne"),
                new Card("Moscow", "Paris"),
                new Card("Cologne", "Moscow"),
                new Card("Paris", "Singapore"),
                new Card("Johannesburg", "Melbourne"),
            };

            Card[] sorted = SortTravel(cards);

            Assert.IsTrue(IsSorted(sorted));
        }

        [TestMethod]

        public void SortTravel_PresortedArray_Sorted()
        {
            Card[] cards = new Card[]
            {
                new Card("Johannesburg", "Melbourne"),
                new Card("Melbourne", "Cologne"),
                new Card("Cologne", "Moscow"),
                new Card("Moscow", "Paris"),
                new Card("Paris", "Singapore"),
            };

            Card[] sorted = SortTravel(cards);

            Assert.IsTrue(IsSorted(sorted));
        }

        public void SortTravel_ReversedArray_Sorted()
        {
            Card[] cards = new Card[]
            {
                new Card("Paris", "Singapore"),
                new Card("Moscow", "Paris"),
                new Card("Cologne", "Moscow"),
                new Card("Melbourne", "Cologne"),
                new Card("Johannesburg", "Melbourne"),
            };

            Card[] sorted = SortTravel(cards);

            Assert.IsTrue(IsSorted(sorted));
        }

        [TestMethod]

        public void SortTravel_EmptyArray_ReturnsEmptyArray()
        {
            Card[] cards = new Card[0];

            Card[] sorted = SortTravel(cards);

            Assert.AreEqual(0, sorted.Length);
        }

        [ExpectedException(typeof(ArgumentException)), TestMethod]

        public void SortTravel_NullReferenceArray_ExceptionThrown()
        {
            Card[] sorted = SortTravel(null);
        }

        [TestMethod]

        public void SortTravel_SingleElementArray_EqualArray()
        {
            Card[] cards = new Card[] { new Card("Paris", "Singapore") };

            Card[] sorted = SortTravel(cards);

            Assert.AreEqual(cards, sorted);
        }

        private bool IsSorted(Card[] cards)
        {
            for (var i = 0; i < cards.Length - 1; i++)
            {
                if (cards[i].ToCity != cards[i + 1].FromCity)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
