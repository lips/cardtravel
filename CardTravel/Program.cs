﻿using System;
using System.Linq;

namespace CardTravel
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Card[] cards = new Card[]
            {
                new Card("Melbourne", "Cologne"),
                new Card("Moscow", "Paris"),
                new Card("Cologne", "Moscow"),
            };

            Card[] sorted = SortTravel(cards);

            foreach (var card in sorted)
            {
                Console.WriteLine(card);
            }

            Console.ReadLine();
        }

        public static Card[] SortTravel(Card[] cards)
        {
            Func<Card[], Card[], int, int> findPreviousCard = null;
            Func<Card[], Card[], int, int> findFollowingCard = null;

            if (cards == null)
            {
                throw new ArgumentException();
            }

            if (cards.Length == 0 || cards.Length == 1)
            {
                return cards;
            }

            Card[] ordered = new Card[(cards.Length * 2) - 1];
            int starting = cards.Length - 1;
            ordered[starting] = cards[0];

            findPreviousCard = (dictionary, buff, position) =>
            {
                var found = dictionary.Where(c => c.ToCity == buff[position].FromCity).ToArray();
                if (found.Length > 0)
                {
                    position--;
                    buff[position] = found[0];
                    return findPreviousCard(dictionary, buff, position);
                }
                else
                {
                    return position;
                }
            };

            findFollowingCard = (dictionary, buff, position) =>
            {
                var found = dictionary.Where(c => c.FromCity == buff[position].ToCity).ToArray();
                if (found.Length > 0)
                {
                    position++;
                    buff[position] = found[0];
                    return findFollowingCard(dictionary, buff, position);
                }
                else
                {
                    return position;
                }
            };

            int head = findPreviousCard(cards, ordered, starting);
            int tail = findFollowingCard(cards, ordered, starting);

            return ordered.Skip(head).Take(cards.Length).ToArray();
        }
    }

    public struct Card
    {
        public readonly string FromCity;
        public readonly string ToCity;

        public Card(string FromCity, string ToCity)
        {
            this.FromCity = FromCity;
            this.ToCity = ToCity;
        }

        public override string ToString()
        {
            return FromCity + ':' + ToCity;
        }
    }
}
